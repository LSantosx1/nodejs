const express = require('express');
const app = express();

app.route('/').get(function(req, res){
    res.send('Hello World');
});

app.listen(3000, function(){
    console.log('Server está rodando na porta 3000');
});